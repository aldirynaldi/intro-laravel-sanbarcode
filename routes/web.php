<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'HomeControlleer@Home');

// Route::get('/register', function () {
//     return view('page.register');
// });

Route::get('/register', 'AuthController@Register');

// Route::get('/welcome', function () {
//     return view('page.welcome');
// });

Route::post('/welcome', 'AuthController@Welcome');
