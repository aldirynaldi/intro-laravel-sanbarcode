<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First Name:</label> <br><br>
        <input type="text" name="firstname"><br><br>

        <label for="lastname">Last Name:</label> <br><br>
        <input type="text" name="lastname"><br><br>

        <label for="gender">Gender</label> <br><br>
        <input type="radio" name="gn">Male<br>
        <input type="radio" name="gn">Female<br>
        <input type="radio" name="gn">Other<br><br>

        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="american">American</option>
            <option value="spanish">Spanish</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>

        <label>Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="submit">

    </form>
</body>
</html>