<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register() {
        return view('page.register');
    }

    public function Welcome(Request $request) {
        //dd($request->all());
        $firstName = $request['firstname'];
        $lastName = $request['lastname'];

        return view('page.welcome', compact('firstName', 'lastName'));
    }
}
